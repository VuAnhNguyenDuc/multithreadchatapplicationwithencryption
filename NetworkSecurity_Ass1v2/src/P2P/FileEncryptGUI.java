package P2P;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.crypto.spec.SecretKeySpec;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Algorithms.CryptoException;
import Algorithms.CryptoUtils;
import Algorithms.RSA;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.crypto.SecretKey;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class FileEncryptGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtSecretKeyLink;

	static ObjectInputStream ois;
	static ObjectOutputStream oos;
	static String filePath;
	static CryptoUtils cryptoUtils;
	static SecretKey secretKey;
	static RSA rsaUtils = new RSA();

	FileEncryptGUI ui;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FileEncryptGUI frame = new FileEncryptGUI(ois,oos,filePath);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FileEncryptGUI(ObjectInputStream ois, ObjectOutputStream oos, String filePath) {

		FileEncryptGUI.ois = ois;
		FileEncryptGUI.oos = oos;
		FileEncryptGUI.filePath = filePath;

		setTitle("File Sending Manager");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 540, 309);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent arg0) {
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Choose encryption type : ");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel.setBounds(23, 22, 146, 34);
		contentPane.add(lblNewLabel);

		String[] encryptAlgorithms = {"AES","DES"};

		@SuppressWarnings("unchecked")
		JComboBox algorithmChooser = new JComboBox(encryptAlgorithms);
		algorithmChooser.setBounds(182, 29, 86, 20);
		contentPane.add(algorithmChooser);

		JLabel lblNewLabel_1 = new JLabel("Secret Key Link :");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel_1.setBounds(27, 123, 103, 17);
		contentPane.add(lblNewLabel_1);

		JLabel keyGenStatus = new JLabel("Status : ");
		keyGenStatus.setFont(new Font("Times New Roman", Font.BOLD, 13));
		keyGenStatus.setBounds(174, 85, 306, 14);
		contentPane.add(keyGenStatus);

		JTextField txtSecretKeyLink = new JTextField();
		txtSecretKeyLink.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtSecretKeyLink.setBounds(132, 121, 284, 20);
		contentPane.add(txtSecretKeyLink);
		txtSecretKeyLink.setColumns(10);
		contentPane.add(txtSecretKeyLink);

		JLabel sendKeyStatus = new JLabel("Status : ");
		sendKeyStatus.setFont(new Font("Times New Roman", Font.BOLD, 13));
		sendKeyStatus.setBounds(174, 177, 306, 14);
		contentPane.add(sendKeyStatus);

		JButton btnSendKey = new JButton("Send Secret Key");
		btnSendKey.setEnabled(false);
		btnSendKey.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					sendKeyStatus.setText("Status : Sending...");
					String algorithm =(String) algorithmChooser.getSelectedItem();

					ChatMessage chatMessage;

					// Gui truc tiep file qua
					/*
					String stringKey = new String(Files.readAllBytes(Paths.get(txtSecretKeyLink.getText())));
					byte[] byteKey = Base64.getDecoder().decode(stringKey);
					secretKey = new SecretKeySpec(byteKey, 0, byteKey.length, algorithm);
					chatMessage = new ChatMessage("FILE_SEND", algorithm, true, byteKey);
					*/
					Path path = Paths.get(txtSecretKeyLink.getText());
					byte[] data = Files.readAllBytes(path);
					/*
					String stringKey = new String(Files.readAllBytes(Paths.get(txtSecretKeyLink.getText())));
					byte[] byteKey = Base64.getDecoder().decode(stringKey);
					//secretKey = new SecretKeySpec(byteKey, 0, byteKey.length, algorithm);*/

					String stringKey = new String(Files.readAllBytes(path));
					byte[] byteKey = Base64.getDecoder().decode(stringKey);
					secretKey = new SecretKeySpec(byteKey, 0, byteKey.length, algorithm);

					chatMessage = new ChatMessage("FILE_SEND",algorithm,true,data);


					chatMessage.content = rsaUtils.encryptData(new String(chatMessage.content,"UTF-8"));
					oos.writeObject(chatMessage);
					oos.flush();
					sendKeyStatus.setText("Status : Done...");
				} catch (Exception e2) {
					// TODO: handle exception
					e2.printStackTrace();
				}

			}
		});
		btnSendKey.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnSendKey.setBounds(17, 173, 129, 23);
		contentPane.add(btnSendKey);

		JLabel sendFileStatus = new JLabel("Status : ");
		sendFileStatus.setFont(new Font("Times New Roman", Font.BOLD, 13));
		sendFileStatus.setBounds(174, 223, 306, 14);
		contentPane.add(sendFileStatus);

		JButton btnSendFile = new JButton("Send");
		btnSendFile.setEnabled(false);
		btnSendFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					sendFileStatus.setText("Status : Sending...");
					String algorithm =(String) algorithmChooser.getSelectedItem();
					File inputFile = new File(filePath);
					FileInputStream inputStream;
					inputStream = new FileInputStream(inputFile);
					Path path = Paths.get(filePath);
					byte[] inputBytes = Files.readAllBytes(path);
					inputStream.read(inputBytes);

					if(cryptoUtils == null){
						if(algorithm.equals("AES")){
							cryptoUtils = new CryptoUtils("AES","AES");
						} else{
							cryptoUtils = new CryptoUtils("DES", "DES/CBC/PKCS5Padding");
						}
					}

					// Send MD5 hash
					MessageDigest md = MessageDigest.getInstance("MD5");
					String digest = MD5Utils.getDigest(new FileInputStream(filePath),md,2048);

					byte[] outputBytes = cryptoUtils.encrypt(secretKey, inputBytes);
					inputStream.close();

					ChatMessage chatMessage = new ChatMessage("FILE_SEND", algorithm, false, outputBytes);
					oos.writeObject(chatMessage);
					oos.flush();

					ChatMessage md5 = new ChatMessage("MD5_Hash","",false,rsaUtils.encryptData(digest));
					oos.writeObject(md5);
					oos.flush();

					String extension = "";

					int i = filePath.lastIndexOf('.');
					if (i > 0) {
						extension = filePath.substring(i+1);
					}
					ChatMessage fileExtension = new ChatMessage("FILE_EXT", "", false, rsaUtils.encryptData(extension));
					oos.writeObject(fileExtension);
					oos.flush();

					sendFileStatus.setText("Status : Done...");
				} catch (IOException | NoSuchAlgorithmException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSendFile.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnSendFile.setBounds(17, 219, 129, 23);
		contentPane.add(btnSendFile);

		JButton btnGenKey = new JButton("Generate Key");
		btnGenKey.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnGenKey.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				keyGenStatus.setText("Status : Generating...");
				String encryptAlgorithm = (String) algorithmChooser.getSelectedItem();
				if(encryptAlgorithm.equals("AES")){
					cryptoUtils = new CryptoUtils("AES", "AES");
				} else{
					cryptoUtils = new CryptoUtils("DES", "DES/CBC/PKCS5Padding");
				}
				secretKey = cryptoUtils.generateKey();
				Base64.Encoder myEncoder = Base64.getEncoder().withoutPadding();
				String encodedKey = myEncoder.encodeToString(secretKey.getEncoded());
				try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("secretKey.txt"))) {
					writer.write("");
					writer.write(encodedKey);
					writer.close();
					String temp = System.getProperty("user.dir")+"/secretKey.txt";
					txtSecretKeyLink.setText(temp);
					btnSendKey.setEnabled(true);
					btnSendFile.setEnabled(true);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				keyGenStatus.setText("Status : Key generated as file in context path");

			}
		});
		btnGenKey.setBounds(17, 81, 129, 23);
		contentPane.add(btnGenKey);

		JButton btnChooseKeyFile = new JButton("...");
		btnChooseKeyFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.showDialog(ui, "Select File");
				File file = fileChooser.getSelectedFile();

				if(file != null){
					if(!file.getName().isEmpty()){
						btnSendFile.setEnabled(true);  btnSendKey.setEnabled(true); String str;
						str = file.getPath();
						txtSecretKeyLink.setText(str);
					}
				}
			}
		});
		btnChooseKeyFile.setBounds(426, 120, 54, 23);
		contentPane.add(btnChooseKeyFile);


	}
}
