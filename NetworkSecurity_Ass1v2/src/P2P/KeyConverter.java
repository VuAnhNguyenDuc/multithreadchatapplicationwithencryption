package P2P;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

public class KeyConverter {

	// SecretKey -> Byte[] ->(Base64 Encode) String
	// (Base64Decode) String -> Byte[] -> SecretKeySpec


	public static String SecretKeyToString(SecretKey secretKey) throws GeneralSecurityException{
		return Base64.getEncoder().encodeToString(secretKey.getEncoded());
	}

	public static SecretKey StringToSecretKey(String encodedKey, String algorithm){
		byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
		SecretKey originalKey = new SecretKeySpec(decodedKey,0,decodedKey.length, algorithm);
		return originalKey;
	}

	public static PrivateKey loadPrivateKey(String key64) throws GeneralSecurityException {
	    byte[] clear = Base64.getDecoder().decode(key64); //base64Decode(key64);
	    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
	    KeyFactory fact = KeyFactory.getInstance("RSA");
	    PrivateKey priv = fact.generatePrivate(keySpec);
	    Arrays.fill(clear, (byte) 0);
	    return priv;
	}


	public static PublicKey loadPublicKey(String stored) throws GeneralSecurityException {
	    byte[] data = Base64.getDecoder().decode(stored); //base64Decode(stored);
	    X509EncodedKeySpec spec = new X509EncodedKeySpec(data);
	    KeyFactory fact = KeyFactory.getInstance("RSA");
	    return fact.generatePublic(spec);
	}

	public static String savePrivateKey(PrivateKey priv) throws GeneralSecurityException {
	    KeyFactory fact = KeyFactory.getInstance("RSA");
	    PKCS8EncodedKeySpec spec = fact.getKeySpec(priv,
	            PKCS8EncodedKeySpec.class);
	    byte[] packed = spec.getEncoded();
	    String key64 = Base64.getEncoder().encodeToString(packed);

	    Arrays.fill(packed, (byte) 0);
	    return key64;
	}


	public static String savePublicKey(PublicKey publ) throws GeneralSecurityException {
	    KeyFactory fact = KeyFactory.getInstance("RSA");
	    X509EncodedKeySpec spec = fact.getKeySpec(publ,
	            X509EncodedKeySpec.class);
	    return Base64.getEncoder().encodeToString(spec.getEncoded()); //base64Encode(spec.getEncoded());
	}


	public static void main(String[] args) throws Exception {
	    KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
	    KeyPair pair = gen.generateKeyPair();

	    String pubKey = savePublicKey(pair.getPublic());
	    PublicKey pubSaved = loadPublicKey(pubKey);
	    System.out.println(pair.getPublic()+"\n"+pubSaved);

	    String privKey = savePrivateKey(pair.getPrivate());
	    PrivateKey privSaved = loadPrivateKey(privKey);
	    System.out.println(pair.getPrivate()+"\n"+privSaved);
	}
}
