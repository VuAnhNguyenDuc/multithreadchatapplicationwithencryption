package P2P;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class MD5Checker extends JFrame {

    private JPanel contentPane;
    private JTextField txtHashCode;
    private JTextField txtChooseFile;
    private JTextField txtResult;
    static MD5Checker ui;
    private JTextField txtFileMD5;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MD5Checker frame = new MD5Checker();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public MD5Checker() {
        setTitle("MD5 Checker");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 524, 313);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblEnterMdCode = new JLabel("MD5 Code :");
        lblEnterMdCode.setFont(new Font("Times New Roman", Font.BOLD, 13));
        lblEnterMdCode.setBounds(10, 33, 98, 33);
        contentPane.add(lblEnterMdCode);

        txtHashCode = new JTextField();
        txtHashCode.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtHashCode.setBounds(82, 39, 397, 20);
        contentPane.add(txtHashCode);
        txtHashCode.setColumns(10);

        JLabel lblFilePath = new JLabel("File Path :");
        lblFilePath.setFont(new Font("Times New Roman", Font.BOLD, 13));
        lblFilePath.setBounds(10, 100, 98, 33);
        contentPane.add(lblFilePath);

        txtChooseFile = new JTextField();
        txtChooseFile.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtChooseFile.setBounds(82, 106, 333, 20);
        contentPane.add(txtChooseFile);
        txtChooseFile.setColumns(10);

        JButton btnChooseFile = new JButton("...");
        btnChooseFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.showDialog(ui, "Select File");
                File file = fileChooser.getSelectedFile();

                if(file != null){
                    if(!file.getName().isEmpty()){
                        String str;
                        str = file.getPath();
		                /*
		                if(txtChooseFile.getText().length() > 30){
		                    String t = file.getPath();
		                    str = t.substring(0, 20) + " [...] " + t.substring(t.length() - 20, t.length());
		                }
		                else{
		                    str = file.getPath();
		                }*/
                        txtChooseFile.setText(str);
                    }
                }
            }
        });
        btnChooseFile.setBounds(425, 105, 54, 23);
        contentPane.add(btnChooseFile);

        txtResult = new JTextField();
        txtResult.setBorder(new LineBorder(new Color(171, 173, 179)));
        txtResult.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtResult.setEditable(false);
        txtResult.setBounds(82, 232, 397, 20);
        contentPane.add(txtResult);
        txtResult.setColumns(10);

        JButton btnCheck = new JButton("Check");
        btnCheck.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                try {
                    MessageDigest md = MessageDigest.getInstance("MD5");
                    File file = new File(txtChooseFile.getText());
                    String fileDigest = MD5Utils.getDigest(new FileInputStream(file), md, 2048);
                    txtFileMD5.setText(fileDigest);
                    if(fileDigest.equals(txtHashCode.getText())){
                        txtResult.setText("MD5 match!");
                    } else{
                        txtResult.setText("MD5 does not match!");
                    }
                } catch (NoSuchAlgorithmException | FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });
        btnCheck.setFont(new Font("Times New Roman", Font.BOLD, 13));
        btnCheck.setBounds(212, 150, 89, 23);
        contentPane.add(btnCheck);

        JLabel lblResult = new JLabel("Result :");
        lblResult.setFont(new Font("Times New Roman", Font.BOLD, 13));
        lblResult.setBounds(10, 226, 98, 33);
        contentPane.add(lblResult);

        JLabel lblFileMd = new JLabel("File MD5:");
        lblFileMd.setFont(new Font("Times New Roman", Font.BOLD, 13));
        lblFileMd.setBounds(10, 189, 98, 33);
        contentPane.add(lblFileMd);

        txtFileMD5 = new JTextField();
        txtFileMD5.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtFileMD5.setEditable(false);
        txtFileMD5.setColumns(10);
        txtFileMD5.setBorder(new LineBorder(new Color(171, 173, 179)));
        txtFileMD5.setBounds(82, 195, 397, 20);
        contentPane.add(txtFileMD5);

    }
}
