package P2P;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.net.InetAddress;
import java.net.ServerSocket;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.net.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.JTextPane;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.io.*;

import javax.swing.JLabel;

import Algorithms.RSA;
import Client.getUserInfo;
import javax.swing.JSeparator;
import java.awt.Font;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.PublicKey;

public class Server extends JFrame {

    private JPanel contentPane;
    static ServerSocket server;
    static Socket client;
    static ObjectInputStream ois;
    static ObjectOutputStream oos;
    static String Userserver;
    static String Userconnect;
    Server ui;
    public JTextField txtSend;
    public JTextArea txtChat;
    public JTextField txtName;
    public JTextField txtLinkFile;
    public JButton btnOpenFile;
    public JButton btnSendFile;
    public JTextField txtSaveLink;
    public JLabel lblNewLabel;
    public JLabel lblNewLabel_1;
    public JLabel lblNewLabel_2;
    static File file;
    static KeyPair myKeyPair;
    static PublicKey senderPubKey;
    private JButton btnDecryptManager;
    private JButton btnMD5Checker;


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Server frame = new Server(Userserver, server, client);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Server(String Userserver, ServerSocket server, Socket client) {
        setFont(new Font("Times New Roman", Font.BOLD, 13));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent arg0) {
                try {
                    ois.close();
                    oos.flush();
                    oos.close();

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dispose();
            }
        });
        ui = this;
        Server.server = server;
        Server.client = client;
        Server.Userserver = Userserver;

        serverThread t = new serverThread(server, client, this , null);
        t.start();

        //setTitle(Userconnect + " is talking to you...");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 645, 470);
        setResizable(false);
        contentPane = new JPanel();
        contentPane.setFont(new Font("Times New Roman", Font.BOLD, 13));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        txtChat = new JTextArea();
        txtChat.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtChat.setBorder(new LineBorder(new Color(0, 0, 0)));
        txtChat.setBounds(10, 11, 619, 269);
        contentPane.add(txtChat);
        txtChat.setEditable(false);

        txtSend = new JTextField();
        txtSend.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtSend.setBorder(new LineBorder(Color.BLACK));
        txtSend.setBounds(66, 334, 340, 20);
        contentPane.add(txtSend);
        txtSend.setColumns(10);

        JButton btnSend = new JButton("SEND");
        btnSend.setFont(new Font("Times New Roman", Font.BOLD, 13));
        btnSend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                ChatMessage chatMessage = new ChatMessage("text","",false,txtSend.getText().getBytes(StandardCharsets.UTF_8));
                t.sendChatMessage(chatMessage);
                txtChat.append("Me : " + txtSend.getText() + "\n");
                txtSend.setText("");
            }
        });
        btnSend.setBounds(416, 333, 89, 23);
        contentPane.add(btnSend);

        txtName = new JTextField();
        txtName.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtName.setBorder(new LineBorder(Color.BLACK));
        txtName.setBounds(10, 301, 175, 23);
        contentPane.add(txtName);
        txtName.setText(" My name is " + Userserver);
        txtName.setEditable(false);
        txtName.setColumns(10);

        txtLinkFile = new JTextField();
        txtLinkFile.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtLinkFile.setBorder(new LineBorder(Color.BLACK));
        txtLinkFile.setBounds(66, 365, 340, 20);
        contentPane.add(txtLinkFile);
        txtLinkFile.setColumns(10);
        txtLinkFile.setEditable(false);

        btnOpenFile = new JButton("...");
        btnOpenFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.showDialog(ui, "Select File");
                file = fileChooser.getSelectedFile();

                if(file != null){
                    if(!file.getName().isEmpty()){
                        btnSendFile.setEnabled(true); String str;
                        str = file.getPath();
                        txtLinkFile.setText(str);
                    }
                }
            }
        });
        btnOpenFile.setBounds(416, 364, 51, 23);
        contentPane.add(btnOpenFile);

        btnSendFile = new JButton("SEND FILE");
        btnSendFile.setFont(new Font("Times New Roman", Font.BOLD, 13));
        btnSendFile.setEnabled(false);
        btnSendFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {

                    FileEncryptGUI fileEncryptGUI = new FileEncryptGUI(Server.ois, Server.oos , txtLinkFile.getText());
                    fileEncryptGUI.setVisible(true);
                } catch (Exception e2) {
                    // TODO: handle exception
                }
            }
        });
        btnSendFile.setBounds(477, 364, 116, 23);
        contentPane.add(btnSendFile);


        txtSaveLink = new JTextField();
        txtSaveLink.setFont(new Font("Times New Roman", Font.BOLD, 13));
        txtSaveLink.setBorder(new LineBorder(Color.BLACK));
        txtSaveLink.setBounds(66, 396, 439, 20);
        contentPane.add(txtSaveLink);
        txtSaveLink.setColumns(10);
        txtSaveLink.setEditable(false);


        lblNewLabel = new JLabel("Message:");
        lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 13));
        lblNewLabel.setBounds(10, 337, 63, 14);
        contentPane.add(lblNewLabel);

        lblNewLabel_1 = new JLabel("File Link:");
        lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 13));
        lblNewLabel_1.setBounds(10, 368, 63, 14);
        contentPane.add(lblNewLabel_1);

        lblNewLabel_2 = new JLabel("Save Link:");
        lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 13));
        lblNewLabel_2.setBounds(10, 399, 63, 14);
        contentPane.add(lblNewLabel_2);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 291, 619, 2);
        contentPane.add(separator);

        btnDecryptManager = new JButton("DECRYPT MANAGER");
        btnDecryptManager.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FileDecryptGUI fileDecryptGUI = new FileDecryptGUI();
                fileDecryptGUI.setVisible(true);
            }
        });
        btnDecryptManager.setFont(new Font("Times New Roman", Font.BOLD, 13));
        btnDecryptManager.setBounds(243, 301, 163, 23);
        contentPane.add(btnDecryptManager);

        btnMD5Checker = new JButton("MD5 CHECKER");
        btnMD5Checker.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MD5Checker md5Checker = new MD5Checker();
                md5Checker.setVisible(true);
            }
        });
        btnMD5Checker.setFont(new Font("Times New Roman", Font.BOLD, 13));
        btnMD5Checker.setBounds(416, 299, 133, 23);
        contentPane.add(btnMD5Checker);
    }

    class serverThread extends Thread {
        ObjectInputStream ois;
        ObjectOutputStream oos;
        ServerSocket server;
        Socket client;
        Server ui;
        RSA rsaUtils = new RSA();


        serverThread(ServerSocket server, Socket client, Server ui, PublicKey senderPubKey) {
            // this.Userconnected = Userconnected;
            this.server = server;
            this.client = client;
            this.ui = ui;
            this.Open();
        }

        public void Open() {
            try {
                oos = new ObjectOutputStream(client.getOutputStream());
                oos.flush();
                ois = new ObjectInputStream(client.getInputStream());
                Server.oos = oos;
                Server.ois = ois;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public void sendPubKey(){
            try {
                // Generate RSA public and private key then send the public to the receiver
                /// Generate RSA public and private keys and save them to files
                RSA rsaUtil = new RSA();
                KeyPair keyPair = rsaUtil.generateKeyPair();
                rsaUtil.generateKeyFiles(keyPair.getPublic(), keyPair.getPrivate());
                Server.myKeyPair = keyPair;

                // Convert the public key to string
                String pubKey = KeyConverter.savePublicKey(keyPair.getPublic());
                ChatMessage myPubKey = new ChatMessage("RSA_Pub_Key", "", false, pubKey.getBytes(StandardCharsets.UTF_8));
                //sendMsg(pubKey);
                oos.writeObject(myPubKey);
                oos.flush();
            } catch (GeneralSecurityException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public void sendChatMessage(ChatMessage chatMessage){
            try {
                chatMessage.content = rsaUtils.encryptData(new String(chatMessage.content,"UTF-8"));
                oos.writeObject(chatMessage);
                oos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void sendFile(String algorithm, String filePath){
            try {
                File file = new File(filePath);
                byte[] content = Files.readAllBytes(file.toPath());
                if(algorithm == "DES"){

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            // TODO Auto-generated method stub
            try {
                while(true){
                    ChatMessage input = (ChatMessage) ois.readObject();
                    if(input.messageType.equals("RSA_Pub_Key")){
                        Server.senderPubKey = KeyConverter.loadPublicKey(new String(input.content,"UTF-8"));
                        ui.txtChat.append("Public key received! \n");
                        sendPubKey();
                    }
                    else if(input.messageType.equals("text")){
                        String decryptedMsg = rsaUtils.decryptData(input.content);
                        ui.txtChat.append(Userconnect + " : " + decryptedMsg + "\n");
                    }
                    else if(input.messageType.equals("client_name")){
                        Server.Userconnect = new String(input.content,"UTF-8");
                        ui.setTitle(Userconnect + " is talking to you...");
                    }
                    else if(input.messageType.equals("FILE_SEND")){
                        if(input.isSecretKey){
                            if(JOptionPane.showConfirmDialog(ui, Server.Userconnect + " wants to send you the secret key file. Accept?") == 0){

                                JFileChooser jf = new JFileChooser();
                                jf.showDialog(ui, "Select File");
                                //File file = jf.getSelectedFile();

                                String saveTo = jf.getSelectedFile().getPath();

                                String decryptedMsg = rsaUtils.decryptData(input.content);
                                ui.txtChat.append(Server.Userconnect + " used the algorithm : " + input.encrypType + "\n");
                                File encryptedFile = new File(saveTo);

                                // Delete the content of the file first
                                PrintWriter writer = new PrintWriter(encryptedFile);
                                writer.print("");
                                writer.close();

                                // Then we write the new key
                                FileOutputStream outputStream = new FileOutputStream(encryptedFile);
                                outputStream.write(decryptedMsg.getBytes(StandardCharsets.UTF_8));
                                //outputStream.write(input.content);
                                outputStream.close();
                                ui.txtChat.append(Server.Userconnect + " sent you the secret key to the path "  + saveTo + "\n");
                            }
                        } else{
                            if(JOptionPane.showConfirmDialog(ui, Server.Userconnect + " wants to send you the encrypted file. Accept?") == 0){

                                JFileChooser jf = new JFileChooser();
                                jf.setSelectedFile(new File(ui.txtSaveLink.getText()));
                                int returnVal = jf.showSaveDialog(ui);

                                String saveTo = jf.getSelectedFile().getPath();
                                ui.txtSaveLink.setText(saveTo);

                                File encryptedFile = new File(ui.txtSaveLink.getText());
                                FileOutputStream outputStream = new FileOutputStream(encryptedFile);
                                outputStream.write(input.content);
                                outputStream.close();
                                ui.txtChat.append("The encrypted file has been written to the link : " + ui.txtSaveLink.getText() + "\n");
                            }
                        }
                    } else if(input.messageType.equals("MD5_Hash")){
                        String decryptedMsg = rsaUtils.decryptData(input.content);
                        ui.txtChat.append(Userconnect + " send the MD5 hash for the file : " + decryptedMsg + "\n");
                    } else if(input.messageType.equals("FILE_EXT")){
                        String extension = rsaUtils.decryptData(input.content);
                        ui.txtChat.append("The original extension of the file was " + extension + " \n");
                    }
                }
            } catch (IOException | ClassNotFoundException | GeneralSecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    class Upload_Server extends Thread{
        public String IP;
        public int port;
        public Socket client;
        public FileInputStream In;
        public OutputStream Out;
        public File file;
        public Server ui;
        public Upload_Server(String IP,int port,File filePath,Server ui) {
            // TODO Auto-generated constructor stub
            try {
                this.file = filePath;
                this.ui = ui;
                client = new Socket(IP,port);
                Out = client.getOutputStream();
                In = new FileInputStream(filePath);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        @Override
        public void run() {
            // TODO Auto-generated method stub
            try {
                byte[] buffer = new byte[1024];
                int count;

                while((count = In.read(buffer)) >=0){
                    Out.write(buffer,0,count);
                }
                Out.flush();
                ui.txtChat.append("File upload completed! \n");
                //wait(5000);
                //ui.txtSaveLink.setText("");

                if(In != null){
                    In.close();
                }
                if(Out != null){
                    Out.close();
                }
                if(client != null){
                    client.close();
                }
            } catch (Exception e) {
                // TODO: handle exception
                ui.txtChat.append("Cannot Upload this file! \n");
                e.printStackTrace();
            }
        }
    }

    class Download_Server extends Thread{
        public ServerSocket server;
        public Socket client;
        public int port;
        public String saveTo = "";
        public DataInputStream In;
        public FileOutputStream Out;
        public Server ui;

        public Download_Server(String saveTo,Server ui) {
            // TODO Auto-generated constructor stub
            try {
                server = new ServerSocket(8000);
                this.saveTo = saveTo;
                this.ui = ui;
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        @Override
        public void run() {
            // TODO Auto-generated method stub
            try {
                client = server.accept();
                ui.txtChat.append("Download: " + client.getRemoteSocketAddress() + "\n" );

                In = new DataInputStream(client.getInputStream());
                Out = new FileOutputStream(saveTo);

                byte[] buffer = new byte[1024];
                int count;

                while((count = In.read(buffer)) >= 0){
                    Out.write(buffer, 0, count);
                }
                Out.flush();

                ui.txtChat.append("Download completed! \n");
                ui.txtLinkFile.setText("");
                if(Out != null){
                    Out.close();
                }
                if(In != null) {
                    In.close();
                }
                if(client != null){
                    client.close();
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }

}



/*
			int i = 1;

			while (true) {
				String input = dis.readUTF();
				getUserInfo name = new getUserInfo(input);

				if (i == 1) {
					ui.setTitle(name.getHeadPart() + " is talking to you...");
					Userconnected = name.getHeadPart();
				} else {


					//Client gui qua server
					if(input.equals("FILE_SEND_REQ")){
						if(JOptionPane.showConfirmDialog(ui, Userconnected + " wants to send you a file. Accept?" ) == 0){
							JFileChooser jf = new JFileChooser();
							jf.setSelectedFile(new File(ui.txtSaveLink.getText()));
							int returnVal = jf.showSaveDialog(ui);

							String saveTo = jf.getSelectedFile().getPath();
							ui.txtSaveLink.setText(saveTo);

							if(saveTo != null  && returnVal == JFileChooser.APPROVE_OPTION){
								dos.writeUTF("OK_SEND_FILE");
								Download_Server t = new Download_Server(saveTo,ui);
								t.start();
							}
						}
					}
					// server gui qua client
					else if(input.equals("OK_SEND_FILE")){
						String filePath = ui.txtLinkFile.getText();
						File myFile = new File(filePath);

						String IP = name.getLastPart();
						//System.out.println(IP);

						Upload_Server up = new Upload_Server(IP,8000,myFile,ui);
						up.start();
					}
					else{
                        byte[] encryptedMsg = input.getBytes(StandardCharsets.UTF_8);
                        String result = rsaUtils.decryptData(encryptedMsg);
						ui.txtChat.append(Userconnected + " : " + input + "\n");
					}
				}
				i++;
			}

			/*
			 input = dis.readUTF();
				if(input.equals("SEND_FILE_REQ")){
					if(JOptionPane.showConfirmDialog(ui, this.Userconnect + " wants to send you a file. Accept?" ) == 0){
						JFileChooser jf = new JFileChooser();
						jf.setSelectedFile(new File(ui.txtSaveLink.getText()));
						int returnVal = jf.showSaveDialog(ui);

						String saveTo = jf.getSelectedFile().getPath();
						ui.txtSaveLink.setText(saveTo);

						if(saveTo != null  && returnVal == JFileChooser.APPROVE_OPTION){
							dos.writeUTF("OK_SEND_FILE");
							Download_Client t = new Download_Client(saveTo,ui);
							t.start();
						}
					}
				}

				if(input.equals("OK_SEND_FILE")){
					String filePath = ui.txtLinkFile.getText();
					File myFile = new File(filePath);
					String IP = ui.IP;
					Upload_Client up = new Upload_Client(IP,8000,myFile,ui);
					up.start();
				}
				else{
					ui.txtChat.append(Me + " : " + input + "\n");
				}
			 */
