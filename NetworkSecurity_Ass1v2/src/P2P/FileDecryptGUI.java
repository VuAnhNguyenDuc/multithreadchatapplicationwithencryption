package P2P;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Algorithms.CryptoException;
import Algorithms.CryptoUtils;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.awt.event.ActionEvent;

public class FileDecryptGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtSecretKey;
	private JTextField txtFileLink;
	private JTextField txtSaveLink;
	FileDecryptGUI ui;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FileDecryptGUI frame = new FileDecryptGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FileDecryptGUI() {
		setTitle("Decrypt Manager");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 540, 309);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Secret Key Link");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel.setBounds(29, 43, 102, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("File Link");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel_1.setBounds(29, 80, 89, 14);
		contentPane.add(lblNewLabel_1);

		String[] algorithms = {"AES","DES"};
		JComboBox algorithmChooser = new JComboBox(algorithms);
		algorithmChooser.setBounds(141, 121, 71, 20);
		contentPane.add(algorithmChooser);

		JLabel decryptStatus = new JLabel("Status : ");
		decryptStatus.setFont(new Font("Times New Roman", Font.BOLD, 13));
		decryptStatus.setBounds(142, 214, 352, 14);
		contentPane.add(decryptStatus);

		txtSecretKey = new JTextField();
		txtSecretKey.setBounds(141, 40, 306, 20);
		contentPane.add(txtSecretKey);
		txtSecretKey.setColumns(10);

		txtFileLink = new JTextField();
		txtFileLink.setBounds(141, 77, 306, 20);
		contentPane.add(txtFileLink);
		txtFileLink.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Algorithm");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel_2.setBounds(29, 124, 89, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel("Save Link");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblNewLabel_4.setBounds(29, 169, 89, 14);
		contentPane.add(lblNewLabel_4);

		txtSaveLink = new JTextField();
		txtSaveLink.setBounds(141, 166, 306, 20);
		contentPane.add(txtSaveLink);
		txtSaveLink.setColumns(10);

		JButton btnSelectSecretKey = new JButton("...");
		btnSelectSecretKey.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.showDialog(ui, "Select File");
				File file = fileChooser.getSelectedFile();

				if(file != null){
					if(!file.getName().isEmpty()){
						String str;
						str = file.getPath();
						txtSecretKey.setText(str);
					}
				}
			}
		});
		btnSelectSecretKey.setBounds(457, 39, 37, 23);
		contentPane.add(btnSelectSecretKey);

		JButton btnSelectFile = new JButton("...");
		btnSelectFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.showDialog(ui, "Select File");
				File file = fileChooser.getSelectedFile();

				if(file != null){
					if(!file.getName().isEmpty()){
						String str;
						str = file.getPath();
						txtFileLink.setText(str);
					}
				}
			}
		});
		btnSelectFile.setBounds(457, 76, 37, 23);
		contentPane.add(btnSelectFile);

		JButton btnSelectSaveLink = new JButton("...");
		btnSelectSaveLink.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.showDialog(ui, "Select File");
				File file = fileChooser.getSelectedFile();

				if(file != null){
					if(!file.getName().isEmpty()){
						String str;
						str = file.getPath();
						txtSaveLink.setText(str);
					}
				}
			}
		});
		btnSelectSaveLink.setBounds(457, 165, 37, 23);
		contentPane.add(btnSelectSaveLink);

		JButton btnDecrypt = new JButton("Decrypt");
		btnDecrypt.setEnabled(true);
		btnDecrypt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				decryptStatus.setText("Status : Decrypting...");
				String algorithm = (String) algorithmChooser.getSelectedItem();
				CryptoUtils cryptoUtils;
				if(algorithm.equals("AES")){
					cryptoUtils = new CryptoUtils(algorithm, "AES");
				} else{
					cryptoUtils = new CryptoUtils(algorithm, "DES/CBC/PKCS5Padding");
				}
				try {
					// get the secret key
					String stringKey = new String(Files.readAllBytes(Paths.get(txtSecretKey.getText())));
					byte[] byteKey = Base64.getDecoder().decode(stringKey);
					SecretKey secretKey = new SecretKeySpec(byteKey, 0, byteKey.length, algorithm);

					// get the encrypted file
					Path path = Paths.get(txtFileLink.getText());
					byte[] fileByte = Files.readAllBytes(path);

					byte[] decryptedFile = cryptoUtils.decrypt(secretKey, fileByte);

					FileOutputStream fos = new FileOutputStream(txtSaveLink.getText());
					fos.write(decryptedFile);
					fos.close();

					decryptStatus.setText("Status : Done.");
				} catch (IOException | CryptoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDecrypt.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnDecrypt.setBounds(29, 210, 89, 23);
		contentPane.add(btnDecrypt);
	}

}
