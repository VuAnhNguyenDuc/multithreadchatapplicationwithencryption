package P2P;

import javax.crypto.SecretKey;
import javax.swing.*;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Vu Anh Nguyen Duc on 3/22/2017.
 */
public class MD5Utils {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String getDigest(InputStream is, MessageDigest md, int byteArraySize){
        try {
            md.reset();
            byte[] bytes = new byte[byteArraySize];
            int numBytes;
            while((numBytes = is.read(bytes)) != -1){
                md.update(bytes,0,numBytes);
            }
            byte[] digest = md.digest();
            return new String(bytesToHex(digest));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static void main(String[] args){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            Path path = Paths.get("E:\\My Workspace\\Intellij 2016\\NetworkSecurity_Ass1v2\\secretKey.txt");
            File file = new File("E:\\My Workspace\\Intellij 2016\\NetworkSecurity_Ass1v2\\secretKey.txt");
            String digest = getDigest(new FileInputStream(file),md,2048);
            System.out.println(digest);
        } catch (NoSuchAlgorithmException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
