package P2P;

import java.io.Serializable;

public class ChatMessage implements Serializable {
	private static final Long serialVersionUID = 1L;
	
	// message type : text, file_sending , RSA_Pub_Key
	// encrypType : AES, DES, " " (rsa la khi gui key)
	// is SecretKey : boolean
	// sender
	// content ( chinh la secret key khi type la file_sending)
	// recipient
	
	public String messageType, encrypType;

	public byte[] content;

	public boolean isSecretKey;
	
	/*
	 * 
	 */
	
	public ChatMessage(String messageType, String encrypType, Boolean isSecretKey,byte[] content){
		this.messageType = messageType;
		this.encrypType = encrypType;
		this.isSecretKey = isSecretKey;
		this.content = content;
	}
	
	@Override
	public String toString(){
		return "{messageType='"+messageType+"', encrypType='"+encrypType+"', isSecretKey='"+isSecretKey+"', content = '"+content+"'}";
	}

}
