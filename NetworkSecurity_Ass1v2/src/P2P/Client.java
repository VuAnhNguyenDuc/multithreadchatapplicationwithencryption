package P2P;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JScrollPane;


import Algorithms.RSA;
import Client.getUserInfo;


import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Font;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.PublicKey;

public class Client extends JFrame {
	// Su dung cac bien IP server, port 5k, Userconnect, client
	private JPanel contentPane;
	public JTextField txtSend;
	public JTextArea txtChat;
	static Socket client;
	static String Userconnect;
	static String IP;
	static ObjectInputStream ois;
	static ObjectOutputStream oos;
	public JTextField txtName;
	static String Me;
	public JTextField txtLinkFile;
	public JButton btnSend;
	public JButton btnSendFile;
	public JButton btnBrowse;
	public File file;
	public JTextField txtSaveLink;
	static KeyPair myKeyPair;
	private JButton btnDecryptManager;
	private JButton btnMD5Checker;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client frame = new Client(Userconnect,IP,Me);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Button Browse(...) Clicked Event
	private void btnBrowseAction(ActionEvent evt) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showDialog(this, "Select File");
		file = fileChooser.getSelectedFile();

		if(file != null){
			if(!file.getName().isEmpty()){
				btnSendFile.setEnabled(true); String str;
				str = file.getPath();
				txtLinkFile.setText(str);
			}
		}
	}

	/**
	 * Create the frame.
	 */
	public Client(String Userconnect,String IP,String Me) {
		setFont(new Font("Times New Roman", Font.BOLD, 13));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				try {
					ois.close();
					oos.flush();
					oos.close();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dispose();
			}
		});
		setIconImage(Toolkit.getDefaultToolkit().getImage("D:\\Eclipse Lunar\\Work Space\\C-S\\image\\server_icon.png"));

		Client.Userconnect = Userconnect;
		Client.IP = IP;
		Client.Me = Me;

		setTitle("Talking to " + Userconnect);
		try {
			client = new Socket(IP,5000);
			oos = new ObjectOutputStream(client.getOutputStream());
			oos.flush();
			ois = new ObjectInputStream(client.getInputStream());
			Client.oos = oos;
			Client.ois = ois;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		clientThread t = new clientThread(Userconnect, ois, oos, client, this, Me, null);
		t.start();
		t.sendClientName();
		t.sendPubKey();

		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 645, 470);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtChat = new JTextArea();
		txtChat.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtChat.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtChat.setBounds(10, 11, 619, 269);
		contentPane.add(txtChat);
		txtChat.setEditable(false);

		txtSend = new JTextField();
		txtSend.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtSend.setBorder(new LineBorder(Color.BLACK));
		txtSend.setBounds(66, 334, 337, 20);
		contentPane.add(txtSend);
		txtSend.setColumns(10);

		btnSend = new JButton("SEND");
		btnSend.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ChatMessage chatMessage = new ChatMessage("text","",false,txtSend.getText().getBytes(StandardCharsets.UTF_8));
				t.sendChatMessage(chatMessage);
				txtChat.append("Me : " + txtSend.getText() + "\n");
				txtSend.setText("");
			}
		});
		btnSend.setBounds(416, 333, 89, 23);
		contentPane.add(btnSend);

		txtName = new JTextField();
		txtName.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtName.setBorder(new LineBorder(Color.BLACK));
		txtName.setBounds(10, 301, 175, 23);
		contentPane.add(txtName);
		txtName.setColumns(10);
		txtName.setText(" My name is " + Me);
		txtName.setEditable(false);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 291, 619, 2);
		contentPane.add(separator);

		JLabel lblMessage = new JLabel("Message:");
		lblMessage.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblMessage.setBounds(10, 337, 59, 14);
		contentPane.add(lblMessage);

		JLabel lblFilename = new JLabel("File Link:");
		lblFilename.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblFilename.setBounds(10, 368, 59, 14);
		contentPane.add(lblFilename);

		txtLinkFile = new JTextField();
		txtLinkFile.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtLinkFile.setBorder(new LineBorder(Color.BLACK));
		txtLinkFile.setBounds(66, 365, 337, 20);
		txtLinkFile.setEditable(false);
		contentPane.add(txtLinkFile);
		txtLinkFile.setColumns(10);

		btnBrowse = new JButton("...");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				btnBrowseAction(evt);
			}
		});
		btnBrowse.setBounds(416, 364, 51, 23);
		contentPane.add(btnBrowse);

		btnSendFile = new JButton("SEND FILE");
		btnSendFile.setFont(new Font("Times New Roman", Font.BOLD, 12));
		btnSendFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					FileEncryptGUI fileEncryptGUI = new FileEncryptGUI(Client.ois, Client.oos , txtLinkFile.getText());
					fileEncryptGUI.setVisible(true);
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		});
		btnSendFile.setEnabled(false);
		btnSendFile.setBounds(476, 364, 114, 23);
		contentPane.add(btnSendFile);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 495, 269);
		contentPane.add(scrollPane);

		txtSaveLink = new JTextField();
		txtSaveLink.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtSaveLink.setBorder(new LineBorder(Color.BLACK));
		txtSaveLink.setBounds(66, 396, 439, 20);
		contentPane.add(txtSaveLink);
		txtSaveLink.setColumns(10);
		txtSaveLink.setEditable(false);

		JLabel lblSaveLink = new JLabel("Save Link:");
		lblSaveLink.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblSaveLink.setBounds(10, 399, 70, 14);
		contentPane.add(lblSaveLink);

		btnDecryptManager = new JButton("DECRYPT MANAGER");
		btnDecryptManager.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileDecryptGUI fileDecryptGUI = new FileDecryptGUI();
				fileDecryptGUI.setVisible(true);
			}
		});
		btnDecryptManager.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnDecryptManager.setBounds(240, 300, 163, 23);
		contentPane.add(btnDecryptManager);

		btnMD5Checker = new JButton("MD5 CHECKER");
		btnMD5Checker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MD5Checker md5Checker = new MD5Checker();
				md5Checker.setVisible(true);
			}
		});
		btnMD5Checker.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnMD5Checker.setBounds(416, 299, 131, 23);
		contentPane.add(btnMD5Checker);
	}
}

class clientThread extends Thread {
	Socket client;
	String Userconnect;
	ObjectInputStream ois;
	ObjectOutputStream oos;
	Client ui;
	String Me;
	public getUserInfo sta;
	static RSA rsaUtils = new RSA();

	PublicKey senderPubKey;

	public clientThread(String Userconnect, ObjectInputStream ois, ObjectOutputStream oos, Socket client, Client ui,String Me, PublicKey senderPubKey) {
		// TODO Auto-generated constructor stub
		this.Userconnect = Userconnect;
		this.client = client;
		this.ois = ois;
		this.oos = oos;
		this.ui = ui;
		this.Me = Me;
	}

	public void sendChatMessage(ChatMessage chatMessage){
		try {
			chatMessage.content = rsaUtils.encryptData(new String(chatMessage.content,"UTF-8"));
			oos.writeObject(chatMessage);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendPubKey() {
		try {
			// Generate RSA public and private key then send the public to the receiver
			/// Generate RSA public and private keys and save them to files
			RSA rsaUtil = new RSA();
			KeyPair keyPair = rsaUtil.generateKeyPair();
			rsaUtil.generateKeyFiles(keyPair.getPublic(), keyPair.getPrivate());
			Client.myKeyPair = keyPair;

			// Convert the public key to string
			String pubKey = KeyConverter.savePublicKey(keyPair.getPublic());
			ChatMessage myPubKey = new ChatMessage("RSA_Pub_Key", "", false, pubKey.getBytes(StandardCharsets.UTF_8));
			oos.writeObject(myPubKey);
			oos.flush();
		} catch (GeneralSecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendClientName(){
		try {
			ChatMessage chatMessage = new ChatMessage("client_name", "",false,Me.getBytes(StandardCharsets.UTF_8));
			oos.writeObject(chatMessage);
			oos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			while(true){
				ChatMessage input = (ChatMessage) ois.readObject();
				if(input.messageType.equals("RSA_Pub_Key")){
					this.senderPubKey = KeyConverter.loadPublicKey(new String(input.content,"UTF-8"));
					ui.txtChat.append("Public key received! \n");
				}
				else if(input.messageType.equals("text")){
					String decrypedMsg = rsaUtils.decryptData(input.content);
					ui.txtChat.append(Userconnect + " : " + decrypedMsg + "\n");
				}
				else if(input.messageType.equals("FILE_SEND")){
					if(input.isSecretKey){
						if(JOptionPane.showConfirmDialog(ui, Client.Userconnect + " wants to send you the secret key file. Accept?") == 0){

							JFileChooser jf = new JFileChooser();
							jf.showDialog(ui, "Select File");
							//File file = jf.getSelectedFile();

							String saveTo = jf.getSelectedFile().getPath();

							String decryptedMsg = rsaUtils.decryptData(input.content);
							ui.txtChat.append(Client.Userconnect + " used the algorithm : " + input.encrypType + "\n");
							File encryptedFile = new File(saveTo);

							// Delete the content of the file first
							PrintWriter writer = new PrintWriter(encryptedFile);
							writer.print("");
							writer.close();

							// Then we write the new key
							FileOutputStream outputStream = new FileOutputStream(encryptedFile);
							outputStream.write(decryptedMsg.getBytes(StandardCharsets.UTF_8));
							outputStream.close();
							ui.txtChat.append(Client.Userconnect + " sent you the secret key to the path "  + saveTo + "\n");
						}
					} else{
						if(JOptionPane.showConfirmDialog(ui, Client.Userconnect + " wants to send you the encrypted file. Accept?") == 0){

							JFileChooser jf = new JFileChooser();
							jf.setSelectedFile(new File(ui.txtSaveLink.getText()));
							int returnVal = jf.showSaveDialog(ui);

							String saveTo = jf.getSelectedFile().getPath();
							ui.txtSaveLink.setText(saveTo);

							File encryptedFile = new File(ui.txtSaveLink.getText());
							FileOutputStream outputStream = new FileOutputStream(encryptedFile);
							outputStream.write(input.content);
							ui.txtChat.append("The encrypted file has been written to the link : " + ui.txtSaveLink.getText() + "\n");
							outputStream.close();
						}
					}
				}
				else if(input.messageType.equals("MD5_Hash")){
					String decryptedMsg = rsaUtils.decryptData(input.content);
					ui.txtChat.append(Userconnect + " send the MD5 hash for the file : " + decryptedMsg + "\n");
				}
				else if(input.messageType.equals("FILE_EXT")){
					String extension = rsaUtils.decryptData(input.content);
					ui.txtChat.append("The original extension of the file was " + extension + " \n");
				}
			}
		} catch (IOException|ClassNotFoundException|GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class Upload_Client extends Thread{
	public String IP;
	public int port;
	public Socket client;
	public FileInputStream In;
	public OutputStream Out;
	public File file;
	public Client ui;
	public Upload_Client(String IP,int port,File filePath,Client ui) {
		// TODO Auto-generated constructor stub
		try {
			this.file = filePath;
			this.ui = ui;
			client = new Socket(IP,port);
			Out = client.getOutputStream();
			In = new FileInputStream(filePath);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			byte[] buffer = new byte[1024];
			int count;

			while((count = In.read(buffer)) >=0){
				Out.write(buffer,0,count);
			}
			Out.flush();
			ui.txtChat.append("File upload completed! \n");

			if(In != null){
				In.close();
			}
			if(Out != null){
				Out.close();
			}
			if(client != null){
				client.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
			ui.txtChat.append("Cannot Upload this file! \n");
			e.printStackTrace();
		}
	}
}

class Download_Client extends Thread{
	public ServerSocket server;
	public Socket client;
	public int port;
	public String saveTo = "";
	public DataInputStream In;
	public FileOutputStream Out;
	public Client ui;

	public Download_Client(String saveTo,Client ui) {
		// TODO Auto-generated constructor stub
		try {
			server = new ServerSocket(8000);
			this.saveTo = saveTo;
			this.ui = ui;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			client = server.accept();
			ui.txtChat.append("Download: " + client.getRemoteSocketAddress() + "\n" );

			In = new DataInputStream(client.getInputStream());
			Out = new FileOutputStream(saveTo);

			byte[] buffer = new byte[1024];
			int count;

			while((count = In.read(buffer)) >= 0){
				Out.write(buffer, 0, count);
			}
			Out.flush();

			ui.txtChat.append("Download completed! \n");

			if(Out != null){
				Out.close();
			}
			if(In != null) {
				In.close();
			}
			if(client != null){
				client.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}

/*
				if(i == 1){
					try {
						// Receive server's pub key
						// Generate RSA public and private key then send the public to the receiver
						/// Generate RSA public and private keys and save them to files
						RSA rsaUtil = new RSA();
						KeyPair keyPair = rsaUtil.generateKeyPair();
						rsaUtil.generateKeyFiles(keyPair.getPublic(), keyPair.getPrivate());
						Client.myKeyPair = keyPair;

						// Convert the public key to string
						String pubKey = KeyConverter.savePublicKey(keyPair.getPublic());
						//ChatMessage myPubKey = new ChatMessage("RSA_Pub_Key", "", false, ui.Userconnect, pubKey, ui.Me);
						sendMsg(pubKey);
					} catch (GeneralSecurityException e) {
						e.printStackTrace();
					}
				}
				if(input.equals("FILE_SEND_REQ")){
					if(JOptionPane.showConfirmDialog(ui, Userconnect + " wants to send you a file. Accept?" ) == 0){
						JFileChooser jf = new JFileChooser();
						jf.setSelectedFile(new File(ui.txtSaveLink.getText()));
						int returnVal = jf.showSaveDialog(ui);

						String saveTo = jf.getSelectedFile().getPath();
						ui.txtSaveLink.setText(saveTo);

						if(saveTo != null  && returnVal == JFileChooser.APPROVE_OPTION){
							dos.writeUTF("OK_SEND_FILE");
							Download_Client t = new Download_Client(saveTo,ui);
							t.start();
						}
					}
				}

				else if(input.equals("OK_SEND_FILE")){
					String filePath = ui.txtLinkFile.getText();
					File myFile = new File(filePath);
					String IP = ui.IP;
					Upload_Client up = new Upload_Client(IP,8000,myFile,ui);
					up.start();
				}
				else{
                    byte[] encryptedMsg = input.getBytes(StandardCharsets.UTF_8);
                    String result = rsaUtils.decryptData(encryptedMsg);
					ui.txtChat.append("VuAnh " + " : " + input + "\n"); ///Cho nay
				}
				i++;
				//sta = new StringtoArray(input);
 */