package Client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JSeparator;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JList;
import javax.swing.JScrollPane;

import P2P.Client;
import Server.Message;

import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JPasswordField;
import javax.swing.JTextArea;

import java.awt.Font;

import javax.swing.SwingConstants;

public class ChatFrame extends JFrame {
	
	//Variables
	public SocketClient client;
	public int port;
	public Socket sock;
	public String serverAddr, username, password;
	public Thread clientThread;
	public DefaultListModel model;
	public File file;
	public String historyFile = "D:/History.xml";
	public HistoryFrame historyFrame;
	public History hist;
	public DataInputStream dis;
	public DataOutputStream dos;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			System.out.println("Look & Feel exception");
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new ChatFrame().setVisible(true);
			}
		});
	}

	public boolean isWin32() {
		return System.getProperty("os.name").startsWith("Windows");
	}
	
	/**
	 * Create the frame.
	 */
	public ChatFrame() {
		addWindowListener(new WindowAdapter() {
			@Override public void windowOpened(WindowEvent e) {}
            @Override public void windowClosing(WindowEvent e) { 
            	try{
            		client.send(new Message("message", username, ".bye", "SERVER", InetAddress.getLocalHost().getHostAddress())); 
            		clientThread.stop();  
            		}catch(Exception ex){
            		}
            	}
            @Override public void windowClosed(WindowEvent e) {}
            @Override public void windowIconified(WindowEvent e) {}
            @Override public void windowDeiconified(WindowEvent e) {}
            @Override public void windowActivated(WindowEvent e) {}
            @Override public void windowDeactivated(WindowEvent e) {}
		});
		setIconImage(Toolkit.getDefaultToolkit().getImage("D:\\Eclipse Lunar\\Work Space\\C-S\\image\\server_icon.png"));
		setTitle("Chat Messenger");
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 675, 455);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblHostAddr = new JLabel("Host Address:");
		lblHostAddr.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblHostAddr.setBounds(10, 11, 89, 14);
		contentPane.add(lblHostAddr);
		
		txtHostIP = new JTextField();
		txtHostIP.setBorder(new LineBorder(Color.BLACK));
		txtHostIP.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtHostIP.setText("localhost");
		txtHostIP.setBounds(88, 8, 150, 20);
		contentPane.add(txtHostIP);
		txtHostIP.setColumns(10);
		
		lblHostPort = new JLabel("Host Port:");
		lblHostPort.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblHostPort.setBounds(248, 11, 78, 14);
		contentPane.add(lblHostPort);
		
		txtHostPort = new JTextField();
		txtHostPort.setBorder(new LineBorder(Color.BLACK));
		txtHostPort.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtHostPort.setText("13000");
		txtHostPort.setBounds(316, 8, 150, 20);
		contentPane.add(txtHostPort);
		txtHostPort.setColumns(10);
		
		btnConnect = new JButton("Connect");
		btnConnect.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnConnectAction(e);
			}
		});
		btnConnect.setBounds(476, 7, 78, 23);
		contentPane.add(btnConnect);
		
		lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblUsername.setBounds(10, 36, 68, 14);
		contentPane.add(lblUsername);
		
		lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 13));
		lblPassword.setBounds(248, 36, 78, 14);
		contentPane.add(lblPassword);
		
		txtUsername = new JTextField();
		txtUsername.setBorder(new LineBorder(Color.BLACK));
		txtUsername.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtUsername.setEnabled(false);
		txtUsername.setText("VuAnh");
		txtUsername.setBounds(88, 33, 150, 20);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		btnLogin = new JButton("Log in");
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLogin.setEnabled(false);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					btnLoginAction(e);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnLogin.setBounds(476, 32, 78, 23);
		contentPane.add(btnLogin);
		
		btnSignUp = new JButton("Sign Up");
		btnSignUp.setFont(new Font("Times New Roman", Font.BOLD, 13));
		btnSignUp.setEnabled(false);
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSignUpAction(e);
			}
		});
		btnSignUp.setBounds(560, 32, 89, 23);
		contentPane.add(btnSignUp);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 61, 639, 2);
		contentPane.add(separator);
		
		btnSignOut = new JButton("Sign Out");
		btnSignOut.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		btnSignOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSignOutAction(e);
			}
		});
		btnSignOut.setEnabled(false);
		btnSignOut.setBounds(560, 7, 89, 23);
		contentPane.add(btnSignOut);
		
		listUser = new JList();
		listUser.setFont(new Font("Times New Roman", Font.BOLD, 13));
		listUser.setModel((model = new DefaultListModel()));
		listUser.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent me) {
				if (me.getClickCount() == 2) {
					String str = listUser.getSelectedValue().toString();
					getUserInfo sta = new getUserInfo(str);
					Client connect = new Client(sta.getHeadPart(), sta.getLastPart(),txtUsername.getText());
					connect.setVisible(true);

				}
			}
		});
		listUser.setBorder(new LineBorder(new Color(0, 0, 0)));
		listUser.setBounds(476, 102, 173, 304);
		contentPane.add(listUser);
		
		txtPassword = new JPasswordField();
		txtPassword.setBorder(new LineBorder(Color.BLACK));
		txtPassword.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtPassword.setEnabled(false);
		txtPassword.setText("password");
		txtPassword.setBounds(316, 33, 150, 17);
		contentPane.add(txtPassword);
		
		txtNoti = new JTextArea();
		txtNoti.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtNoti.setEditable(false);
		txtNoti.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtNoti.setBounds(10, 102, 453, 304);
		contentPane.add(txtNoti);
		
		JScrollPane scrNotice = new JScrollPane();
		//scrNotice.setViewportView(txtNotice);
		scrNotice.setBounds(10, 102, 453, 304);
		contentPane.add(scrNotice);
		
		JScrollPane scrList = new JScrollPane();
		//scrList.setViewportView(listUser);
		scrList.setBounds(476, 102, 173, 304);
		contentPane.add(scrList);
		
		lblNewLabel = new JLabel("SERVER NOTIFICATION");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel.setBounds(42, 71, 234, 20);
		contentPane.add(lblNewLabel);
		
		lblUserOnline = new JLabel("USER ONLINE");
		lblUserOnline.setForeground(Color.BLUE);
		lblUserOnline.setHorizontalAlignment(SwingConstants.CENTER);
		lblUserOnline.setFont(new Font("Arial", Font.PLAIN, 18));
		lblUserOnline.setBounds(476, 77, 173, 14);
		contentPane.add(lblUserOnline);
	}
	
	private void btnConnectAction(ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
		serverAddr = txtHostIP.getText();
		port = Integer.parseInt(txtHostPort.getText());

		if (!serverAddr.isEmpty() && !txtHostPort.getText().isEmpty()) {
			try {
				client = new SocketClient(this);
				clientThread = new Thread(client);
				clientThread.start();
				client.send(new Message("test", "testUser", "testContent", "SERVER", null));
			} catch (Exception ex) {
				txtNoti.append("[Application > Me] : Server not found\n");
			}
		}
	}// GEN-LAST:event_jButton1ActionPerformed

	private void btnLoginAction(ActionEvent evt)
			throws IOException {
		username = txtUsername.getText();
		password = txtPassword.getText();

		if (!username.isEmpty() && !password.isEmpty()) {
			client.send(new Message("login", username, password, "SERVER", InetAddress.getLocalHost().getHostAddress()));
		}
	}

	//
	private void btnSignUpAction(ActionEvent evt) {
		username = txtUsername.getText();
		password = txtPassword.getText();

		if (!username.isEmpty() && !password.isEmpty()) {
			client.send(new Message("signup", username, password, "SERVER", null));
		}
	}// GEN-LAST:event_jButton3ActionPerformed

	/*private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton7ActionPerformed
		JFileChooser jf = new JFileChooser();
		jf.showDialog(this, "Select File");

		if (!jf.getSelectedFile().getPath().isEmpty()) {
			historyFile = jf.getSelectedFile().getPath();
			if (this.isWin32()) {
				historyFile = historyFile.replace("/", "\\");
			}
			//txtHisFile.setText(historyFile);
			//txtHisFile.setEditable(false);
			//btnBrowse.setEnabled(false);
			//btnShow.setEnabled(true);
			hist = new History(historyFile);

			historyFrame = new HistoryFrame(hist);
			historyFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			historyFrame.setVisible(false);
		}
	}// GEN-LAST:event_jButton7ActionPerformed

	private void jButton8ActionPerformed(ActionEvent evt) {// GEN-FIRST:event_jButton8ActionPerformed
		historyFrame.setLocation(this.getLocation());
		historyFrame.setVisible(true);
	}// GEN-LAST:event_jButton8ActionPerformed*/

	private void btnSignOutAction(ActionEvent evt){
		try{
			client.send(new Message("message", username, ".bye", "SERVER", InetAddress.getLocalHost().getHostAddress())); 
			clientThread.stop();  
		}catch(Exception ex){
		}
	}

	//Objects
	private JPanel contentPane;
	public JTextField txtHostIP;
	public JTextField txtHostPort;
	public JTextField txtUsername;
	public JTextArea txtNoti;
	private JLabel lblHostAddr;
	private JLabel lblHostPort;
	private JLabel lblUsername;
	private JLabel lblPassword;
	public JButton btnConnect;
	public JButton btnSignOut;
	public JButton btnLogin;
	public JButton btnSignUp;
	private JList listUser;
	public JPasswordField txtPassword;
	private JLabel lblNewLabel;
	private JLabel lblUserOnline;
}
