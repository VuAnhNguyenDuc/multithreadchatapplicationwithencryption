package Client;

import Server.Message;

import java.io.*;
import java.net.*;
import java.util.Date;

import javax.swing.table.DefaultTableModel;

import P2P.ServerListener;

public class SocketClient implements Runnable {

	public int port;
	public String serverAddr;
	public Socket socket;
	public ChatFrame ui;
	public ObjectInputStream In;
	public ObjectOutputStream Out;
	public History hist;

	public SocketClient(ChatFrame frame) throws IOException {
		ui = frame;
		this.serverAddr = ui.serverAddr;
		this.port = ui.port;
		socket = new Socket(InetAddress.getByName(serverAddr), port);

		Out = new ObjectOutputStream(socket.getOutputStream());
		Out.flush();
		In = new ObjectInputStream(socket.getInputStream());
		// System.out.println("");
		hist = ui.hist;
	}

	@Override
	public void run() {
		boolean keepRunning = true;
		while (keepRunning) {
			try {
				Message msg = (Message) In.readObject();
				System.out.println("Incoming : " + msg.toString());
				
				if (msg.type.equals("login")) {
					if (msg.content.equals("TRUE")) {
						ui.btnLogin.setEnabled(false);
						ui.btnSignUp.setEnabled(false);
						ui.txtNoti.append("[SERVER > Me] : Login Successful\n");
						ui.txtUsername.setEnabled(false);
						ui.txtPassword.setEnabled(false);
						ui.btnSignOut.setEnabled(true);
		
						ServerListener t = new ServerListener(ui.txtUsername.getText());
						t.start();
					} else {
						ui.txtNoti.append("[SERVER > Me] : Login Failed\n");
					}
				} else if (msg.type.equals("test")) {
					ui.btnConnect.setEnabled(false);
					ui.btnLogin.setEnabled(true);
					ui.btnSignUp.setEnabled(true);
					ui.txtUsername.setEnabled(true);
					ui.txtPassword.setEnabled(true);
					ui.txtHostIP.setEditable(false);
					ui.txtHostPort.setEditable(false);
					//ui.btnBrowse.setEnabled(true);
				} else if (msg.type.equals("newuser")) {
					if (!msg.content.equals(ui.username)) {
						boolean exists = false;
						for (int i = 0; i < ui.model.getSize(); i++) {
							if (ui.model.getElementAt(i).equals(msg.content)) {
								exists = true;
								break;
							}
						}
						if (!exists) {
							ui.model.addElement(msg.content + "/" + msg.ip);
						}
					}
				} else if (msg.type.equals("signup")) {
					if (msg.content.equals("TRUE")) {
						ui.btnLogin.setEnabled(false);
						ui.btnSignUp.setEnabled(false);
						ui.txtNoti
								.append("[SERVER > Me] : Singup Successful\n");
					} else {
						ui.txtNoti.append("[SERVER > Me] : Signup Failed\n");
					}
				} else if (msg.type.equals("signout")) {
					System.out.println("fuck u!");
					if (msg.content.equals(ui.username)) {
						ui.txtNoti
								.append("[" + msg.sender + " > Me] : Bye\n");
						ui.btnConnect.setEnabled(true);
						ui.txtHostIP.setEditable(true);
						ui.txtHostPort.setEditable(true);

						for (int i = 1; i < ui.model.size(); i++) {
							ui.model.removeElementAt(i);
						}

						ui.clientThread.stop();
					} else {
						ui.model.removeElement(msg.content);
						ui.txtNoti.append("[" + msg.sender + " > All] : "
								+ msg.content + " has signed out\n");
					}
				}
				/*
				 * else if(msg.type.equals("upload_req")){
				 * 
				 * if(JOptionPane.showConfirmDialog(ui,
				 * ("Accept '"+msg.content+"' from "+msg.sender+" ?")) == 0){
				 * 
				 * JFileChooser jf = new JFileChooser(); jf.setSelectedFile(new
				 * File(msg.content)); int returnVal = jf.showSaveDialog(ui);
				 * 
				 * String saveTo = jf.getSelectedFile().getPath(); if(saveTo !=
				 * null && returnVal == JFileChooser.APPROVE_OPTION){ Download
				 * dwn = new Download(saveTo, ui); Thread t = new Thread(dwn);
				 * t.start(); //send(new Message("upload_res",
				 * (""+InetAddress.getLocalHost().getHostAddress()),
				 * (""+dwn.port), msg.sender)); send(new Message("upload_res",
				 * ui.username, (""+dwn.port), msg.sender, null)); } else{
				 * send(new Message("upload_res", ui.username, "NO", msg.sender,
				 * null)); } } else{ send(new Message("upload_res", ui.username,
				 * "NO", msg.sender, null)); } } else
				 * if(msg.type.equals("upload_res")){
				 * if(!msg.content.equals("NO")){ int port =
				 * Integer.parseInt(msg.content); String addr = msg.sender;
				 * 
				 * Upload upl = new Upload(addr, port, ui.file, ui); Thread t =
				 * new Thread(upl); t.start(); } else{
				 * ui.txtNoti.append("[SERVER > Me] : "
				 * +msg.sender+" rejected file request\n"); } }
				 */
				else {
					ui.txtNoti
							.append("[SERVER > Me] : Unknown message type\n");
				}
			} catch (Exception ex) {
				keepRunning = false;
				ui.txtNoti
						.append("[Application > Me] : Connection Failure\n");
				ui.btnConnect.setEnabled(true);
				ui.txtHostIP.setEditable(true);
				ui.txtHostPort.setEditable(true);

				for (int i = 1; i < ui.model.size(); i++) {
					ui.model.removeElementAt(i);
				}

				ui.clientThread.stop();

				System.out.println("Exception SocketClient run()");
				ex.printStackTrace();
			}
		}
	}

	public void send(Message msg) {
		try {
			Out.writeObject(msg);
			Out.flush();
			System.out.println("Outgoing : " + msg.toString());

			/*
			 * if(msg.type.equals("message") && !msg.content.equals(".bye")){
			 * String msgTime = (new Date()).toString(); try{
			 * hist.addMessage(msg, msgTime); DefaultTableModel table =
			 * (DefaultTableModel) ui.historyFrame.jTable1.getModel();
			 * table.addRow(new Object[]{"Me", msg.content, msg.recipient,
			 * msgTime}); } catch(Exception ex){} }
			 */
		} catch (IOException ex) {
			System.out.println("Exception SocketClient send()");
		}
	}

	public void closeThread(Thread t) {
		t = null;
	}
}
