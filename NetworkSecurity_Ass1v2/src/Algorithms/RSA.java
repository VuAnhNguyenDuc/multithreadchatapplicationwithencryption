package Algorithms;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

//import com.ibm.icu.impl.ICUService.Key;

public class RSA {
	
	private static final String PUBLIC_KEY_FILE = "Public.key";
	
	private static final String PRIVATE_KEY_FILE = "Private.key";
	
	// generate a pair of keys
	public KeyPair generateKeyPair(){
		KeyPairGenerator keyPairGenerator;
		try {
			File test1 = new File(PUBLIC_KEY_FILE);
			File test2 = new File(PRIVATE_KEY_FILE);
			
			if(!test1.exists() && !test2.exists()){
				keyPairGenerator = KeyPairGenerator.getInstance("RSA");
				keyPairGenerator.initialize(2048); //1024 used for normal securities
				KeyPair keyPair = keyPairGenerator.generateKeyPair();
				return keyPair;
			} else{
				KeyPair keyPair = new KeyPair(readPublicKeyFromFile(PUBLIC_KEY_FILE), readPrivateKeyFromFile(PRIVATE_KEY_FILE));
				return keyPair;
			}		
		} catch (NoSuchAlgorithmException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void generateKeyFiles(PublicKey publicKey, PrivateKey privateKey){
		try {
			File test1 = new File(PUBLIC_KEY_FILE);
			File test2 = new File(PRIVATE_KEY_FILE);
			
			if(!test1.exists() && !test2.exists()){
				//Pullingout parameters which makes up Key
				   System.out.println("\n------- PULLING OUT PARAMETERS WHICH MAKES KEYPAIR----------\n");
				   KeyFactory keyFactory = KeyFactory.getInstance("RSA");
				   RSAPublicKeySpec rsaPubKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
				   RSAPrivateKeySpec rsaPrivKeySpec = keyFactory.getKeySpec(privateKey, RSAPrivateKeySpec.class);
				   System.out.println("PubKey Modulus : " + rsaPubKeySpec.getModulus());
				   System.out.println("PubKey Exponent : " + rsaPubKeySpec.getPublicExponent());
				   System.out.println("PrivKey Modulus : " + rsaPrivKeySpec.getModulus());
				   System.out.println("PrivKey Exponent : " + rsaPrivKeySpec.getPrivateExponent());
				   
				   //Share public key with other so they can encrypt data and decrypt thoses using private key(Don't share with Other)
				   System.out.println("\n--------SAVING PUBLIC KEY AND PRIVATE KEY TO FILES-------\n");
				   RSA rsaObj = new RSA();
				   rsaObj.saveKeys(PUBLIC_KEY_FILE, rsaPubKeySpec.getModulus(), rsaPubKeySpec.getPublicExponent());
				   rsaObj.saveKeys("public.txt", rsaPubKeySpec.getModulus(), rsaPubKeySpec.getPublicExponent());
				   rsaObj.saveKeys(PRIVATE_KEY_FILE, rsaPrivKeySpec.getModulus(), rsaPrivKeySpec.getPrivateExponent());
				   
				   //Encrypt Data using Public Key
				   //byte[] encryptedData = rsaObj.encryptData("Anuj Patel - Classified Information !");
					  //byte[] encryptedData = rsaObj.encryptData();
				   
				   //Descypt Data using Private Key
				   //rsaObj.decryptData(encryptedData);
			}
			   	   
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	public static void main(String[] args) throws IOException {

		  try {
		   System.out.println("-------GENRATE PUBLIC and PRIVATE KEY-------------");
		   KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		   keyPairGenerator.initialize(2048); //1024 used for normal securities
		   KeyPair keyPair = keyPairGenerator.generateKeyPair();
		   PublicKey publicKey = keyPair.getPublic();
		   PrivateKey privateKey = keyPair.getPrivate();
		   System.out.println("Public Key - " + publicKey);
		   System.out.println("Private Key - " + privateKey);

		   //Pullingout parameters which makes up Key
		   System.out.println("\n------- PULLING OUT PARAMETERS WHICH MAKES KEYPAIR----------\n");
		   KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		   RSAPublicKeySpec rsaPubKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
		   RSAPrivateKeySpec rsaPrivKeySpec = keyFactory.getKeySpec(privateKey, RSAPrivateKeySpec.class);
		   System.out.println("PubKey Modulus : " + rsaPubKeySpec.getModulus());
		   System.out.println("PubKey Exponent : " + rsaPubKeySpec.getPublicExponent());
		   System.out.println("PrivKey Modulus : " + rsaPrivKeySpec.getModulus());
		   System.out.println("PrivKey Exponent : " + rsaPrivKeySpec.getPrivateExponent());
		   
		   //Share public key with other so they can encrypt data and decrypt thoses using private key(Don't share with Other)
		   System.out.println("\n--------SAVING PUBLIC KEY AND PRIVATE KEY TO FILES-------\n");
		   RSA rsaObj = new RSA();
		   rsaObj.saveKeys(PUBLIC_KEY_FILE, rsaPubKeySpec.getModulus(), rsaPubKeySpec.getPublicExponent());
		   rsaObj.saveKeys(PRIVATE_KEY_FILE, rsaPrivKeySpec.getModulus(), rsaPrivKeySpec.getPrivateExponent());
		   
		   //Encrypt Data using Public Key
		   byte[] encryptedData = rsaObj.encryptData("Anuj Patel - Classified Information !");
			  //byte[] encryptedData = rsaObj.encryptData();
		   
		   //Descypt Data using Private Key
		   rsaObj.decryptData(encryptedData);
		   
		  } catch (NoSuchAlgorithmException e) {
		   e.printStackTrace();
		  }catch (InvalidKeySpecException e) {
		   e.printStackTrace();
		  }

		 }
		 */
		
		  /**
		  * Save Files
		  * @param fileName
		  * @param mod
		  * @param exp
		  * @throws IOException
		  */
	public void saveKeys(String fileName,BigInteger mod,BigInteger exp) throws IOException{
		  FileOutputStream fos = null;
		  ObjectOutputStream oos = null;
		  
		  try {
		   System.out.println("Generating "+fileName + "...");
		   fos = new FileOutputStream(fileName);
		   oos = new ObjectOutputStream(new BufferedOutputStream(fos));
		   
		   oos.writeObject(mod);
		   oos.writeObject(exp);   
		   
		   System.out.println(fileName + " generated successfully");
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  finally{
		   if(oos != null){
		    oos.close();
		    
		    if(fos != null){
		     fos.close();
		    }
		   }
		  }  
		 }
		 
		 /**
		  * Encrypt Data
		  * @param data
		  * @throws IOException
		  */
	public byte[] encryptData(String data) throws IOException {
		  byte[] dataToEncrypt = data.getBytes();
		  byte[] encryptedData = null;
		  try {
		   PublicKey pubKey = readPublicKeyFromFile(PUBLIC_KEY_FILE);
		   Cipher cipher = Cipher.getInstance("RSA");
		   cipher.init(Cipher.ENCRYPT_MODE, pubKey);
		   encryptedData = cipher.doFinal(dataToEncrypt);
		  } catch (Exception e) {
		   e.printStackTrace();
		  } 
		  
		  System.out.println("----------------ENCRYPTION COMPLETED------------");  
		  return encryptedData;
		 }

		 /**
		  * Encrypt Data
		  * @param data
		  * @throws IOException
		  */
	public String decryptData(byte[] data) throws IOException {
		  byte[] decryptedData = null;
		  
		  try {
		   PrivateKey privateKey = readPrivateKeyFromFile(PRIVATE_KEY_FILE);
		   Cipher cipher = Cipher.getInstance("RSA");
		   cipher.init(Cipher.DECRYPT_MODE, privateKey);
		   decryptedData = cipher.doFinal(data);

		   //System.out.println("Decrypted Data: " + new String(decryptedData));
			  return new String(decryptedData,"UTF-8");
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		System.out.println("----------------DECRYPTION COMPLETED------------");
		  	return null;

		 }
		 
		 /**
		  * read Public Key From File
		  * @param fileName
		  * @return PublicKey
		  * @throws IOException
		  */
	public PublicKey readPublicKeyFromFile(String fileName) throws IOException{
		  FileInputStream fis = null;
		  ObjectInputStream ois = null;
		  try {
		   fis = new FileInputStream(new File(fileName));
		   ois = new ObjectInputStream(fis);
		   
		   BigInteger modulus = (BigInteger) ois.readObject();
		      BigInteger exponent = (BigInteger) ois.readObject();
		   
		      //Get Public Key
		      RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(modulus, exponent);
		      KeyFactory fact = KeyFactory.getInstance("RSA");
		      PublicKey publicKey = fact.generatePublic(rsaPublicKeySpec);
		            
		      return publicKey;
		      
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  finally{
		   if(ois != null){
		    ois.close();
		    if(fis != null){
		     fis.close();
		    }
		   }
		  }
		  return null;
		 }
		 
		 /**
		  * read Public Key From File
		  * @param fileName
		  * @return
		  * @throws IOException
		  */
	public PrivateKey readPrivateKeyFromFile(String fileName) throws IOException{
		  FileInputStream fis = null;
		  ObjectInputStream ois = null;
		  try {
		   fis = new FileInputStream(new File(fileName));
		   ois = new ObjectInputStream(fis);
		   
		   BigInteger modulus = (BigInteger) ois.readObject();
		      BigInteger exponent = (BigInteger) ois.readObject();
		   
		      //Get Private Key
		      RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(modulus, exponent);
		      KeyFactory fact = KeyFactory.getInstance("RSA");
		      PrivateKey privateKey = fact.generatePrivate(rsaPrivateKeySpec);
		            
		      return privateKey;
		      
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  finally{
		   if(ois != null){
		    ois.close();
		    if(fis != null){
		     fis.close();
		    }
		   }
		  }
		  return null;
		 }
}
