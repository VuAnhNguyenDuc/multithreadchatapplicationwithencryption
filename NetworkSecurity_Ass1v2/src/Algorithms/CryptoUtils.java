package Algorithms;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * A utility class that encrypts or decrypts a file.
 * @author www.codejava.net
 *
 */
public class CryptoUtils {
	public String ALGORITHM;
	public String TRANSFORMATION;
	private static final byte[] iv = {11, 22, 33, 44, 99, 88, 77, 66};

	public CryptoUtils(String ALGORITHM, String TRANSFORMATION){
		this.ALGORITHM = ALGORITHM;
		this.TRANSFORMATION = TRANSFORMATION;
	}

	public SecretKey generateKey(){
		try {
			KeyGenerator keyGen;
			if(ALGORITHM.equals("AES")){
				keyGen = KeyGenerator.getInstance(ALGORITHM);
				keyGen.init(128); // 16 bits
			}else{
				keyGen = KeyGenerator.getInstance(ALGORITHM);
				keyGen.init(56); // 24 bits
			}
			return keyGen.generateKey();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public byte[] doCrypto(int cipherMode, SecretKey key, byte[] inputBytes){
		try {
			//Key secretKey = new SecretKeySpec(key, ALGORITHM);
			Cipher cipher = Cipher.getInstance(TRANSFORMATION);
			if(this.ALGORITHM.equals("AES")){
				cipher.init(cipherMode, key);
			} else{
				AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
				cipher.init(cipherMode, key,paramSpec);
			}

			byte[] outputBytes = cipher.doFinal(inputBytes);
			return outputBytes;

		} catch (NoSuchPaddingException | NoSuchAlgorithmException
				| InvalidKeyException | BadPaddingException
				| IllegalBlockSizeException |InvalidAlgorithmParameterException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public byte[] encrypt(SecretKey key, byte[] inputBytes){
		return doCrypto(Cipher.ENCRYPT_MODE, key, inputBytes);
	}
	public byte[] decrypt(SecretKey key, byte[] inputBytes) throws CryptoException{
		return doCrypto(Cipher.DECRYPT_MODE, key, inputBytes);
	}
	/*
	public void encrypt(SecretKey key, File inputFile, File outputFile)
			throws CryptoException {
		doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
	}

	public void decrypt(SecretKey key, File inputFile, File outputFile)
			throws CryptoException {
		doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
	}

	public void doCrypto(int cipherMode, SecretKey key, File inputFile,
			File outputFile) throws CryptoException {
		try {
			//Key secretKey = new SecretKeySpec(key, ALGORITHM);
			Cipher cipher = Cipher.getInstance(TRANSFORMATION);
			if(this.ALGORITHM.equals("AES")){
				cipher.init(cipherMode, key);
			} else{
				AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
				cipher.init(cipherMode, key,paramSpec);
			}


			FileInputStream inputStream = new FileInputStream(inputFile);
			byte[] inputBytes = new byte[(int) inputFile.length()];
			inputStream.read(inputBytes);

			byte[] outputBytes = cipher.doFinal(inputBytes);

			FileOutputStream outputStream = new FileOutputStream(outputFile);
			outputStream.write(outputBytes);

			inputStream.close();
			outputStream.close();

		} catch (NoSuchPaddingException | NoSuchAlgorithmException
				| InvalidKeyException | BadPaddingException
				| IllegalBlockSizeException | IOException |InvalidAlgorithmParameterException ex) {
			throw new CryptoException("Error encrypting/decrypting file", ex);
		}
	}*/
}
