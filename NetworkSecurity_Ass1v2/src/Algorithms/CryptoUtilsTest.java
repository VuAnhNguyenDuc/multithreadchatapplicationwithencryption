package Algorithms;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

/**
 * A tester for the CryptoUtils class.
 * @author www.codejava.net
 *
 */
public class CryptoUtilsTest {

	//private static final String ALGORITHM = "DES";
	//private static final String TRANSFORMATION = "DES/CBC/PKCS5Padding";

	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES";


	public static void main(String[] args) {
		CryptoUtils utils = new CryptoUtils(ALGORITHM, TRANSFORMATION);
		SecretKey key = utils.generateKey();

		//File inputFile = new File("E:\\My Workspace\\Intellij 2016\\NetworkSecurity_Ass1v2\\test.mp3");
		File encryptedFile = new File("document.encrypted");
		File decryptedFile = new File("document.decrypted");

		try {
			String stringKey = Base64.getEncoder().encodeToString(key.getEncoded());
			FileOutputStream outputStream3 = new FileOutputStream("secretKey.txt");
			outputStream3.write(stringKey.getBytes(StandardCharsets.UTF_8));
			outputStream3.flush();
			outputStream3.close();

			RSA rsaUtils = new RSA();
			byte[] temp1 = rsaUtils.encryptData(stringKey);
			String temp2 = rsaUtils.decryptData(temp1);

			/*
			File inputFile = new File("secretKey.txt");
			FileInputStream inputStream;
			inputStream = new FileInputStream(inputFile);
			Path path = Paths.get("secretKey.txt");
			byte[] inputKeyBytes = Files.readAllBytes(path);
			inputStream.read(inputKeyBytes);*/

			String stringKey1 = new String(Files.readAllBytes(Paths.get("secretKey.txt")));
			byte[] byteKey = Base64.getDecoder().decode(temp2);
			key = new SecretKeySpec(byteKey, 0, byteKey.length, ALGORITHM);

			Path path1 = Paths.get("test.mp3");
			byte[] inputBytes = Files.readAllBytes(path1);
			byte[] encryptedBytes = utils.encrypt(key,inputBytes);
			byte[] decyptedBytes = utils.decrypt(key,encryptedBytes);

			FileOutputStream outputStream1 = new FileOutputStream(encryptedFile);
			outputStream1.write(encryptedBytes);
			outputStream1.close();

			FileOutputStream outputStream = new FileOutputStream(decryptedFile);
			outputStream.write(decyptedBytes);
			outputStream.close();

		} catch (CryptoException e) {
			e.printStackTrace();
		} catch (java.io.IOException e1) {
			e1.printStackTrace();
		}
	}
}

		/*
		String key = "Mary has one cat";
		File inputFile = new File("test.mp3");
		File encryptedFile = new File("document.encrypted");
		File decryptedFile = new File("document.decrypted");

		try {
			CryptoUtils.encrypt(key, inputFile, encryptedFile);
			CryptoUtil
		*/