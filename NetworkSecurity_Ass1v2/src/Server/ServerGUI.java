package Server;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.SwingConstants;

import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JSeparator;


public class ServerGUI extends JFrame {
	public SocketServer server;
	public Thread serverThread;
	public String filePath = "Data.xml";
	
	//java Objects
	public JTextArea txtLink; 
	public JFileChooser fileChooser;
	public JTextArea txtNoti;
	public JPanel contentPane;
	public JList listUser;
	public DefaultListModel model;
	
	//Components
	public JButton btnStart;
	public JButton btnBrowse;
	public JButton btnEnd;
	public JButton btnHistory;
	private JSeparator separator;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServerGUI serFrame = new ServerGUI();
					serFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	/**
	 * Create the frame.
	 */
	public ServerGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("D:\\Eclipse Lunar\\Work Space\\AssNet1\\image\\server_icon.png"));
		setTitle("Server");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 684, 475);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		listUser = new JList();
		listUser.setFont(new Font("Times New Roman", Font.BOLD, 13));
		listUser.setBorder(new LineBorder(Color.GRAY));
		listUser.setModel(model = new DefaultListModel());
		listUser.setBounds(472, 113, 186, 313);
		contentPane.add(listUser);
		
		
		JLabel lblDB = new JLabel("User Database:");
		lblDB.setBounds(10, 11, 89, 15);
		lblDB.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		JLabel lblNoti = new JLabel("NOTIFICATION :");
		lblNoti.setBounds(20, 76, 160, 27);
		lblNoti.setForeground(Color.BLUE);
		lblNoti.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoti.setFont(new Font("Arial", Font.BOLD, 16));
		
		JLabel lblUList = new JLabel("USER LIST :");
		lblUList.setBounds(496, 81, 134, 22);
		lblUList.setForeground(Color.BLUE);
		lblUList.setFont(new Font("Arial", Font.BOLD, 16));
		lblUList.setHorizontalAlignment(SwingConstants.CENTER);
		
		txtLink = new JTextArea();
		txtLink.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtLink.setBounds(109, 8, 308, 22);
		txtLink.setEditable(false);
		txtLink.setBorder(new LineBorder(new Color(255, 0, 0), 1, true));
		
		txtNoti = new JTextArea();
		txtNoti.setBounds(10, 114, 445, 312);
		txtNoti.setEditable(false);
		txtNoti.setFont(new Font("Consolas", 0, 12));
		txtNoti.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	
		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStartAction(e);
			}
		});
		btnStart.setBounds(526, 8, 63, 23);
		btnStart.setEnabled(false);
		btnStart.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		btnEnd = new JButton("End");
		btnEnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				server.stop();
				txtNoti.append("\nServer disconnected.");
				btnStart.setEnabled(true);
				btnEnd.setEnabled(false);
				btnHistory.setEnabled(false);
			}
		});
		btnEnd.setBounds(599, 8, 59, 23);
		btnEnd.setEnabled(false);
		btnEnd.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		btnHistory = new JButton("History");
		btnHistory.setVisible(false);
		btnHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnHistory.setBounds(366, 79, 89, 23);
		btnHistory.setEnabled(false);
		btnHistory.setFont(new Font("Tahoma", Font.PLAIN, 12));

		btnBrowse = new JButton("Browse...");
		btnBrowse.setBounds(427, 8, 89, 23);
		btnBrowse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {				
				fileChooser = new JFileChooser();
				        
		        String[] XML = new String[]{"XML"};
		        fileChooser.setAcceptAllFileFilterUsed(false);
		        fileChooser.addChoosableFileFilter(new SimpleFileFilter(XML, "XML (*.XML)"));		        
		    	fileChooser.showDialog(ServerGUI.this, "Select");
		        File file = fileChooser.getSelectedFile();
		        
		        if(file != null){
		            filePath = file.getPath();
		            if(this.isWin32()){ filePath = filePath.replace("\\", "\\"); }
		            txtLink.setText(filePath);
					btnStart.setEnabled(true);
		        }			
			}
			private boolean isWin32(){
				return System.getProperty("os.name").startsWith("Windows");
			}
		});
		btnBrowse.setFont(new Font("Times New Roman", Font.BOLD, 12));
		
		JScrollPane scrList = new JScrollPane();
		scrList.setBounds(475, 114, 183, 312);
		//scrList.setViewportView(listUser);
		contentPane.add(scrList);
		
		JScrollPane scrNoti = new JScrollPane();
		scrNoti.setBounds(10, 114, 445, 312);
		//scrNoti.setViewportView(txtNoti);
		contentPane.add(scrNoti);
		
		contentPane.setLayout(null);
		contentPane.add(lblDB);
		contentPane.add(txtLink);
		contentPane.add(btnBrowse);
		contentPane.add(btnStart);
		contentPane.add(btnEnd);
		contentPane.add(btnHistory);
		contentPane.add(lblNoti);
		contentPane.add(lblUList);
		contentPane.add(txtNoti);
		
		separator = new JSeparator();
		separator.setBounds(10, 54, 648, 2);
		contentPane.add(separator);
		
	}
	private void btnStartAction(ActionEvent e){
		server = new SocketServer(ServerGUI.this);
		btnStart.setEnabled(false);
		btnBrowse.setEnabled(false);
		btnEnd.setEnabled(true);
		btnHistory.setEnabled(true);
	}
	
	 public void RetryStart(int port){
	        if(server != null){ server.stop(); }
	        server = new SocketServer(ServerGUI.this, port + 1);
	}
}


