package Server;

import java.io.Serializable;

public class Message implements Serializable{
    
    private static final long serialVersionUID = 1L;
    public String type, sender, content, recipient, ip;
    static int temp = 1;
    
    public Message(String type, String sender, String content, String recipient, String IP){
        this.type = type; this.sender = sender; this.content = content; this.recipient = recipient; this.ip = IP;
    }
    
    @Override
    public String toString(){
        return "{type='"+type+"', sender='"+sender+"', content='"+content+"', recipient='"+recipient+"', ip = '"+ip+"'}";
    }
}
