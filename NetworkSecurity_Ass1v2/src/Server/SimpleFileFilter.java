package Server;
import javax.swing.filechooser.*;
import javax.swing.filechooser.FileFilter;

import java.io.*;
//http://tailieu.vn/doc/java-swing-phan-4-914776.html
public class SimpleFileFilter extends FileFilter{
	String[] extensions;
	String description;
	
	public SimpleFileFilter(String ext) {
		this (new String[] {ext}, null);
		}
	
	public SimpleFileFilter(String[] exts,String descr) {
		// TODO Auto-generated constructor stub
		//clone and lowercase the extensions
		extensions = new String[exts.length];
		for(int i = exts.length - 1; i>=0; i-- ){
			extensions[i] = exts[i].toLowerCase();
		}
		//make sure we have a valid (if simplistic) desc
		description = (descr == null ? exts[0] + "files" : descr);
	}
	
	@Override
	public boolean accept(File f) {
		// TODO Auto-generated method stub
		if(f.isDirectory()){return true;}
		String name = f.getName().toLowerCase();
		for(int i = extensions.length - 1; i>=0; i-- ){
			if(name.endsWith(extensions[i])){return true;}
		}
		return false;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return description;
	}
	
}
