# README #

### What is this repository for? ###

* This repository contains a multithread chat and file sending Java application integrated with 3 encrypting and decrypting algorithms (AES,DES,RSA)
* Version 1.0

### Deployment instructions ###

* After download this project, open it using a Java editor like Eclipse or Intellij.
* In the src folder, run file ServerGUI.java located in Server package, you will configure the user database file and start the server with this GUI.
* In Client package, run file ChatFrame.java, you will use this GUI to connect to the server, log in or create a new account.
* If another user is using this chat application, you will see them online. Double click there name to begin talking.
* A new menu will appear, here, you can chat to them, send them encrypted files, decrypting encrypted files, and check the MD5 of that file.
* Database configuration
* How to run tests
* Deployment instructions

### Database configuration ###

* I used xml to store the database of users. In the ServerGUI you will have to input the database file. Just use the Data.xml file I saved here.

### Who do I talk to? ###

* My name is Nguyen Duc Vu Anh,a 4th year CS engineer of the Ho Chi Minh City University of Technology. I have 3 years of coding Java, one year of Maven, Javascript, Ajax, HTML, CSS. 
* If you want to contact me please contact me via this email :vuanhnguyenduc@gmail.com  
* I'm always available to job offers :)